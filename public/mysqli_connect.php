<?php
/**
 * Note When a database is set up on a remote host, this file is placed one level above the root folder for
 * security. Not protecting this file can lead to DOS attacks. Leaving this file located in a public area of the web
 * server will allow anyone to use the code to access the database. Place the file one level above the program
 * code, in an area secured by the web server (or in another secured folder). Use a require statement similar to
 * require('../mysqlconnect.php'); to access your connection information. The examples in this book access the
 * connection file from the same location as the code files. This is done for easy testing but should not be done in
 * a live environment on a publicly accessible web server.
 */

// This file provides the information for accessing the database and connecting to MySQL
// First we define constants:

Define('DB_USER', 'phpuser');
Define('DB_PASSWORD', 'jJk84c@xdb2nCbgN');
Define('DB_HOST', 'db');
Define('DB_NAME', 'php7');

// Next assign database connection to a variable that we will call $dbcon:

try {
    $dbcon = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    mysqli_set_charset($dbcon, 'utf8');
} catch (Exception $e) {
//     print "An Exception occured. Message: " . $e->getMessage();
    print "The system is busy please try later.";
} catch (Error $e) {
//     print "An Error occured. Message: " . $e->getMessage();
    print "The system is busy please try again later.";
}
